from django.urls import path
from django.contrib.auth.views import LoginView
from django.contrib.auth.decorators import login_required
from . import views
urlpatterns = [
    path('signup/', views.Register.as_view(), name="register"),
    path('login/', LoginView.as_view(extra_context={
         'title': 'Login'}, template_name='user/login.html'), name="login"),
    path('profile/', login_required(views.ProfileUpdate.as_view()),
         name="profile-update")
]
