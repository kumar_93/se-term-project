from django.test import SimpleTestCase
from django.urls import reverse, resolve
from proj.views import index
from user.views import Register, ProfileUpdate
from django.contrib.auth.views import LoginView

# Server side test for testing urls


class TestUrls(SimpleTestCase):

    def test_home_url_is_resolved(self):    # E1 edge server server side test

        home_url = reverse('home')
        home_resolved = resolve(home_url)
        self.assertEquals(home_resolved.func, index)
        print('\n\n--Home url tested--')

    def test_login_url_is_resolved(self):   # E2 edge server side test

        login_url = reverse('login')
        login_resolved = resolve(login_url)
        self.assertEquals(login_resolved.func.view_class, LoginView)
        print('--Login url tested--')

    def test_profile_url_is_resolved(self):     # E3 edge server side  test

        profile_url = reverse('profile-update')
        profile_resolved = resolve(profile_url)
        self.assertEquals(profile_resolved.func.view_class, ProfileUpdate)
        print('--Profile url tested--')

    def test_register_url_is_resolved(self):    # E4 edge server side test

        register_url = reverse('register')
        register_resolved = resolve(register_url)
        self.assertEquals(register_resolved.func.view_class, Register)
        print('--Register url tested--\n\n')
