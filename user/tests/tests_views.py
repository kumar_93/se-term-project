from django.test import TestCase, Client
from django.urls import reverse
from user.views import Register, ProfileUpdate
from proj.views import index
from user.models import Profile
from django.contrib.auth.models import User


class TestViews(TestCase):

    def setUp(self):

        self.client = Client()

        self.home_url = reverse('home')
        self.login_url = reverse('login')
        self.register_url = reverse('register')
        self.profile_url = reverse('profile-update')

        response = self.client.post(self.register_url, {
            'first_name': 'aditya',
            'username': 'aditya',
            'email': 'aditya@gmail.com',
            'password1': 'adiadiadi',
            'password2': 'adiadiadi'
        })

        self.username = 'aditya'
        self.password = 'adiadiadi'

    def test_home_view(self):

        response = self.client.get(self.home_url)

        self.assertEquals(response.status_code, 200)    # E2 edge of home view
        # E1 of edge of home view
        self.assertTemplateUsed(response, 'user/index.html')
        print('--home view tested--')

    def test_register_view(self):

        response = self.client.get(self.register_url)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'user/registration.html')

        response = self.client.post(self.register_url, {
            'first_name': 'aditya',
            'username': 'testing123',
            'email': 'aditya.ak@gmail.com',
            'password1': 'adiadiadi',
            'password2': 'adiadiadi'
        })

        self.assertEquals(response.status_code, 302)
        self.assertEquals(response.url, '/user/profile/')

        print('--Registration view tested for ecpected values--')

    def test_login_view(self):

        response = self.client.get(self.login_url)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'user/login.html')

        response = self.client.post(self.login_url, {
            'username': self.username,
            'password': self.password
        })

        self.assertEquals(response.status_code, 302)
        self.assertEquals(response.url, '/user/profile/')

        print('--Login view tested for expected values--')

    def test_profile_view(self):

        self.client.post(self.login_url, {
            'username': self.username,
            'password': self.password
        })

        response = self.client.get(self.profile_url)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'user/new_profile.html')

        pic = open('media/user/profile_pic/default.jpg', 'rb')

        response = self.client.post(self.profile_url, {
            'profile_pic': pic,
            'DOB': '2000-03-27'
        })

        self.assertEquals(response.status_code, 302)
        self.assertEquals(response.url, '/user/profile/')

        pic.close()

        user = User.objects.get(username=self.username)
        profile = user.profile
        self.assertEquals(str(profile.DOB), '2000-03-27')
        print('--Profile view tested for expected values--')

    def test_bypass_profile_view(self):  # Each choice coverage

        self.client.post(self.login_url, {
            'username': self.username,
            'password': self.password
        })

        response = self.client.get(self.profile_url)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'user/new_profile.html')

        pic = open('media/user/profile_pic/default.jpg', 'rb')

        test_cases = [
            {
                'profile_pic': '',
                'dob': '',
                'status_code': 302,
                'response_pp': 'default.jpg',
                'response_dob': 'None'
            },
            {
                'profile_pic': '',
                'dob': '2000-01-01',
                'status_code': 302,
                'response_pp': 'default.jpg',
                'response_dob': '2000-01-01'
            },
            {
                'profile_pic': pic,
                'dob': '2000-02-01',
                'status_code': 302,
                'response_pp': None,
                'response_dob': '2000-02-01'
            },
            {
                'profile_pic': '',
                'dob': '01-02-2000',
                'status_code': 200,
                'response_pp': None,
                'response_dob': '2000-02-01'
            }

        ]

        for test in test_cases:
            response = self.client.post(self.profile_url, {
                'profile_pic': test['profile_pic'],
                'DOB': test['dob'],
            })
            user = User.objects.get(username=self.username)
            profile = user.profile
            self.assertEquals(response.status_code, test['status_code'])
            if (test['response_pp']):
                self.assertEquals(str(profile.profile_pic),
                                  'user/profile_pic/' + test['response_pp'])
            self.assertEquals(str(profile.DOB), test['response_dob'])

        print('--Profile view tested bypass for expected values--')
