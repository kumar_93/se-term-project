from django.shortcuts import render
from .forms import UserRegistrationForm, ProfileForm
from .models import Profile
from django.views.generic.edit import FormView
from django.contrib.auth import login, logout


class Register(FormView):
    form_class = UserRegistrationForm
    template_name = 'user/registration.html'
    extra_context = {'title': 'Sign Up'}
    success_url = '/user/profile/'

    # overriding default methdod copied parent method statements also
    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            user = form.save()
            logout(request)
            login(request, user)
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class ProfileUpdate(FormView):
    form_class = ProfileForm
    template_name = 'user/new_profile.html'
    success_url = '/user/profile/'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Instance variable so that every instance of this do not share the same value
        self.instance = None
        # Stores the instance of the users profile

    def get(self, request, *args, **kwargs):
        # Saving users profile to the instance varoable got initializing form
        self.instance = request.user.profile
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        # Saving users profile to the instance varoable got initializing form
        self.instance = request.user.profile
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        form.save()  # Saving received form data to database
        return super().form_valid(form)

    def get_form_kwargs(self):
        dic = super().get_form_kwargs()
        # Sending instance data for form initilization
        dic.update({'instance': self.instance})
        return dic
